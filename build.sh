#!/bin/bash

# Exit on error
set -e

pushd "$(dirname "$0")"

# Load env
[ -f ".env" ] && . .env
[ -f ".env.local" ] && . .env.local

# Download or Update theme
if [ -d "theme" ]; then
	pushd "theme/"
	git pull
	popd
else
	git clone "${FISSG_THEME_REPO_URL}" "theme"
fi


popd
